// BÀI 1: TÌM SỐ NGUYÊN DƯƠNG NHỎ NHẤT
function sum() {
  var i = 0;
  var total = 0;
  var count = 0;
  for (i = 1; i < 10000; i++) {
    total += i;
    count++;
    if (total > 10000) {
      break;
    }
  }
  document.getElementById(
    "result1"
  ).innerHTML = `Số nguyên dương nhỏ nhất là ${count}`;
}

// BÀI 2 TÍNH TỔNG
function sumXN() {
  var x = document.getElementById("txt-x").value * 1;
  var n = document.getElementById("txt-n").value * 1;
  var sum = 0;
  var pow = 0;
  for (var i = 1; i <= n; i++) {
    pow = Math.pow(x, i);
    sum = sum + pow;
  }
  document.getElementById("result2").innerHTML = `Tổng là: ${sum}`;
}

// BÀI 3: TÍNH GIAI THỪA

function mutiply() {
  var number = document.getElementById("txt-number").value * 1;
  var count = 0;
  var sum1 = 1;

  for (var i = 1; i <= number; i++) {
    sum1 = sum1 * i;
  }

  document.getElementById("result3").innerHTML = ` Tổng giai thừa là: ${sum1}`;
}
// Bài 4: TẠO THẺ DIV

function tabDiv() {
  var content = "";
  for (var i = 1; i <= 10; i++) {
    if (i % 2 == 0) {
      var div = `<div class="bg-primary text-white ">Div Chẳn Nè</div>`;
      content += div;
    } else {
      var div = `<div class="bg-danger text-white ">Div Lẻ Nè</div>`;
      content += div;
    }
  }
  document.getElementById("result4").innerHTML = content;
}
